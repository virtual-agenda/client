import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
// core components
import styles from "assets/jss/material-dashboard-react/components/customInputStyle.js";
import { TextField } from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function CustomDatePicker(props) {
  const classes = useStyles();
  const {
    formControlProps,
    labelText,
    id,
    defaultValue,
    error,
    name,
    onChange,
    value,
    success,
  } = props;

  const underlineClasses = classNames({
    [classes.underlineError]: error,
    [classes.underlineSuccess]: success && !error,
    [classes.underline]: true,
  });

  return (
    <FormControl
      {...formControlProps}
      className={formControlProps.className + " " + classes.formControl}
    >
      <TextField
        id={id}
        type="date"
        label={labelText}
        className={underlineClasses}
        defaultValue={defaultValue}
        name={name}
        onChange={onChange}
        value={value}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </FormControl>
  );
}

CustomDatePicker.propTypes = {
  id: PropTypes.string,
  labelText: PropTypes.node,
  formControlProps: PropTypes.object,
  defaultValue: PropTypes.object,
  error: PropTypes.bool,
  success: PropTypes.bool,
  name: PropTypes.string,
  value: PropTypes.object,
  onChange: PropTypes.object,
};
