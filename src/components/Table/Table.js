import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";

// core components
import styles from "assets/jss/material-dashboard-react/components/tableStyle.js";
import Button from "components/CustomButtons/Button.js";

const useStyles = makeStyles(styles);

const dateFormater = (inDate) => {
  let date = new Date(inDate);
  let format = date.getDate() + "/" + parseInt(date.getMonth()+1) + "/" + date.getFullYear();
  return format;
}

export default function CustomTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, tableHeaderColor, onDelete, onEdit } = props;

  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow className={classes.tableHeadRow}>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData.map((item) => {
            return (
              <TableRow key={item.id} className={classes.tableBodyRow}>
                <TableCell className={classes.tableCell} key={item.id}>
                  {item.id}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {item.title}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {item.description}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {dateFormater(item.dateStart)}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {dateFormater(item.dateEnd)}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {dateFormater(item.dateCreation)}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {item.locationLat}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  {item.locationLng}
                </TableCell>
                <TableCell className={classes.tableCell} key={item.id}>
                  <Button color="info" aria-label="edit" justIcon round onClick={onEdit.bind(this, item)}><Edit/></Button>
                  <Button color="danger" aria-label="edit" justIcon round onClick={onDelete.bind(this, item.id)}><Delete/></Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray",
};

CustomTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray",
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  onDelete: PropTypes.object,
  onEdit: PropTypes.object
};
