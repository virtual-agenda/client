import React, { useEffect, useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { useLocation, useHistory } from "react-router-dom";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};

const useStyles = makeStyles(styles);

export default function TableList() {
  const classes = useStyles();
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  let location = useLocation();
  let history = useHistory();
  let route = "http://localhost:3001/api/event";

  const handleEdit = (item) => {
    history.push("/admin/event", item);
  }

  const handleDelete = (id) => {
    fetch(`${route}\\${id}`, {method: 'DELETE'})
      .then(async response => {
        if(response.ok){
          history.go(0);
        }
      })
  }

  useEffect(() => {
    let queryString = location.state;
    if (queryString) {
      route = `${route}\\${queryString}`;
    }
    setItems([]);
    fetch(route)
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Events Table</h4>
              <p className={classes.cardCategoryWhite}>List of events</p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={[
                  "Id",
                  "Title",
                  "Description",
                  "Date Start",
                  "Date End",
                  "Date Creation",
                  "Latitude",
                  "Longitude",
                  "Actions"
                ]}
                tableData={items}
                onDelete={handleDelete}
                onEdit={handleEdit}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}
