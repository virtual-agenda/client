import React, { useState, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomDatePicker from "components/CustomDatePicker/CustomDatePicker";
import { useLocation, useHistory } from "react-router-dom";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
};

export class MyEvent {
  title;
  description;
  image;
  dateStart;
  dateEnd;
  dateCreation;
  locationLat;
  locationLng;
  constructor() {
    this.title = "";
    this.description = "";
  }
}

const useStyles = makeStyles(styles);

export default function UserProfile() {
  const classes = useStyles();
  const [event, setEvent] = useState({ image: "image1" });
  let location = useLocation();
  let history = useHistory();

  const onChange = (e) => {
    setEvent({ ...event, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if(location.state){
      setEvent(location.state);
    }
  }, []);

  const onSubmit = (e) => {
    if(location.state){
      HandleSaveEdit(e, event.id);
    }else{
      HandleSaveNew(e);
    }
  }

  function HandleSaveEdit(e, id) {
    e.preventDefault();
    fetch(`http://localhost:3001/api/event/${id}`, {
      method: "PUT",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify(event),
    })
      .then((response) => response.json())
      .then((response) => {
        history.push("/admin/table");
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function HandleSaveNew(e) {
    e.preventDefault();
    fetch("http://localhost:3001/api/event", {
      method: "POST",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify(event),
    })
      .then((response) => response.json())
      .then((response) => {
        history.push("/admin/table");
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const dateFormater = (inDate) => {
    let date = new Date(inDate);
    let format = date.getFullYear() + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + date.getDate();
    return format;
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Event Page</h4>
              <p className={classes.cardCategoryWhite}>
                Create or update an Event
              </p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Title"
                    value={event.title}
                    id="title"
                    onChange={onChange}
                    name="title"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomDatePicker
                    labelText="Date Start"
                    id="date-start"
                    value={dateFormater(event.dateStart)}
                    onChange={onChange}
                    name="dateStart"
                    defaultValue="2017-05-24"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomDatePicker
                    labelText="Date End"
                    id="date-end"
                    value={dateFormater(event.dateEnd)}
                    onChange={onChange}
                    name="dateEnd"
                    defaultValue="2017-05-24"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Latitutde"
                    id="latitude"
                    value={event.locationLat}
                    onChange={onChange}
                    name="locationLat"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Longitude"
                    id="longitude"
                    value={event.locationLng}
                    onChange={onChange}
                    name="locationLng"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel style={{ color: "#AAAAAA" }}>
                    Description
                  </InputLabel>
                  <CustomInput
                    labelText="Description of the event"
                    id="description"
                    value={event.description}
                    onChange={onChange}
                    name="description"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                    }}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button color="primary" onClick={onSubmit}>
                Save
              </Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
